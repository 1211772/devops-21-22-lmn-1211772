# DEVOPS

## 1. Class Assignment 2

### 1.1 CA2, Part 1 - Build Tools With Gradle

<br>

**Starting the exercise (CA2 Part 1):**

**1: Copy the code of gradle_basic_commands inside the CA2 Part 1 folder**

- **Creating the CA2 Part 1 directory, README.md and copying the aforementioned repository into this:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>  - `cd D:\devops-21-22-lmn-1211772\`


- **_Creating and copying the files to the new folder d/devops-21-22-lmn-1211772:_**
>  - Creating the directory and the file:
>    - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA2 Part 2\README.md' -ItemType File -Force`**
>  - Copying the aforementioned repository into CA2 Part 1:
>    - **`git clone https://1211772@bitbucket.org/luisnogueira/gradle_basic_demo.git`**
>  - after delete the .git directory
>    - **`rmdir -Force -Recurse .git`**

<br>

**2: Read the instructions available in the readme.md file and experiment with the application**

**Build**
>- **_To build a .jar file with the application:_**
>  - First we have to be in the directory of the application.
>    - **`cd 'D:\devops-21-22-lmn-1211772\CA2 Part 1\gradle_basic_demo\'`**
>  - Then we execute the command:
>    - **`./gradlew build`**

**Run the server**
>- **_In the same folder/directory_**
>  - Execute the following command which starts the server:
>    - **`java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001`**
>  - It informs that the chat server is running

**Run a client**
>- **_Since I am alone at home I executed the client side, on another PowerShell, to check if the server was responding 
and experimented with the app_**
>  - I executed the client side
>    - **`./gradlew runClient`**
>  - As soon as I did this, and inputted my nickname, the other PowerShell, that was still running the server, 
informed me immediately of the new user, which was me, and when I left as well. Confirming that both the client and 
the server were running properly.

<br>

**3: Add a new task to execute the server.**

**Creating a new Task**
>- **_To create the new task I opened the build.gradle file with notepad and edited it to create a new task that 
runs the server in this case:_**
>  - Still in the same directory:
>    - **`notepad build.gradle`**

**_Creating the task in notepad_**

 ```gradle
 task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Runs the server that hosts the chat client"
   classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'
    args '59001'
}
```

<br>

**4: Add a simple unit test and update the gradle script so that it is able to execute the test.**

**Creating the test and dependencies**
>- **_To create the new task I opened the build.gradle file with notepad and edited it to create a new dependency 
to be able to run the tests:_**
>  - Still in the same directory:
>    - **`notepad build.gradle`**

 ```gradle
 dependencies {
    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    implementation 'org.junit.jupiter:junit-jupiter:5.8.1'
    testImplementation 'junit:junit:4.12'
}
```

>- **_After I executed the following command to see if the task test was correctly executed_**
>  - Still in the same directory:
>    - **`./gradlew test`**

<br>

**5: Add a new task of type Copy to be used to make a backup of the sources of the application.**

**Creating a new Task of type Copy**
>- **_To create this task first I need to create a new folder to copy something into it:_**
>  - Still in the same directory:
>    - **`mkdir backup`**
>- To create the new task I opened the build.gradle file with notepad and edited it to create a new task of the type Copy:
>  - Still in the same directory:
>    - **`notepad build.gradle`**

**_Creating the task in notepad_**

 ```gradle
 task copySrc(type: Copy) {
    from 'src'
    into 'backup'
}
```

>- **_After I executed the following command to see if the task test was correctly executed_**
>  - Still in the same directory:
>    - **`./gradlew copySrc`**

<br>

**6: Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application.**

**Creating a new Task of type Zip**
>- **_To create this task I will do the following:_**
>- To create the new task I opened the build.gradle file with notepad and edited it to create a new task of the type Zip:
>  - Still in the same directory:
>    - **`notepad build.gradle`**

**_Creating the task in notepad_**

 ```gradle
 task zipFile(type: Zip) {
    archiveFileName  = 'src.zip'
    from 'src'
    destinationDirectory = file('backup')
}
```

>- **_After I executed the following command to see if the task test was correctly executed_**
>  - Still in the same directory:
>    - **`./gradlew zipFile`**