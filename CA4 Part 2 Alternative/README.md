# DEVOPS

## 1. Class Assignment 4

### 1.1 CA4, Part 2 Alternative - Containers with Docker

<br>

**1: Starting the exercise (CA4 Part 2 Alternative):**

**Create CA4 Part 2 Alternative folder and README.md file**

- **Creating the CA4 Part 2 Alternative directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>    - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA4 Part 2 Alternative\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA4 Part 2 Alternative folder and Readme.md (resolves #46)"`**
>  - **`git push`**

<br>

**2: Choosing the alternative method:**

- **For the alternative options we are discussing two of them and choosing one:**
>  - **1) Kubernetes**
>  - **2) Heroku**

- **We will be discussing the kubernetes option in more detail since that's the alternative that I selected to investigate.**

<br>

**3: 1) Kubernetes:**

**Firstly we need to talk about Docker and Kubernetes to see what's different between the both of them and to know more 
about what they do.**

- **Docker:**
>  - Docker is an open source platform for building, deploying, and managing containerized applications.
>  - It enables developers to package applications into containers—standardized executable components combining application source code with the operating system (OS) libraries and dependencies required to run that code in any environment.
>  - Developers can create containers without Docker, but the platform makes it easier, simpler, and safer to build, deploy and manage containers.
>  - Docker is essentially a toolkit that enables developers to build, deploy, run, update, and stop containers using simple commands and work-saving automation through a single API.
>  - **_The toolkit offers tools such as:_**
>  - **Docker Engine:** is the underlying tooling/client that enables users to easily build, manage, share and run their container objects.
>  - **Dockerfile:** is a text document that contains all the commands a user could call on the command line to assemble an image.
>  - **Docker Hub:** is a hosted repository service provided by Docker for finding and sharing container images with your team.
>  - **Docker Compose:** is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration.
>  - **Docker Swarm Mode:** you can create and test services based upon images you’ve created or other available images. In your production environment, swarm mode provides a fault-tolerant platform with cluster management features to keep your services running and available.

<br>

- **Kubernetes:**
>  - Kubernetes, also known as K8s, is a portable, extensible, open source container orchestration engine for automating deployment, scaling, and management of containerized applications.
>  - It groups containers that make up an application into logical units for easy management and discovery.
>  - Kubernetes provides you with a framework to run distributed systems resiliently.
>  - It takes care of scaling and failover for your application, provides deployment patterns, and more.
>  - **_Kubernetes provides you with:_**
>  - **Service discovery and load balancing:** if traffic to a container is high, Kubernetes is able to load balance and distribute the network traffic so that the deployment is stable.
>  - **Storage orchestration:** automatically mount a storage system of your choice, such as local storages, public cloud providers, and more.
>  - **Automated rollouts and rollbacks:** you can describe the desired state for your deployed containers using Kubernetes, and it can change the actual state to the desired state at a controlled rate.
>  - **Automatic bin packing:** you provide Kubernetes with a cluster of nodes that it can use to run containerized tasks.
>  - **Self-healing:** restarts containers that fail, replaces containers, kills containers that don't respond to your user-defined health check, and doesn't advertise them to clients until they are ready to serve.
>  - **Secret and configuration management:** lets you store and manage sensitive information, such as passwords, OAuth tokens, and SSH keys. You can deploy and update secrets and application configuration without rebuilding your container images, and without exposing secrets in your stack configuration.

<br>

- **Kubernetes architecture:**
>  - **Control Plane:** is the main controlling unit of the cluster, managing its workload and directing communication across the system. It consists of various components, each its own process, that can run both on a single master node or on multiple masters supporting high-availability clusters.
>  - **etcd:** is a persistent, lightweight, distributed, key-value data store.  It reliably stores the configuration data of the cluster, representing the overall state of the cluster at any given point of time. etcd favors consistency over availability in the event of a network partition. The consistency is crucial for correctly scheduling and operating services.
>  - **The API server:** serves the Kubernetes API using JSON over HTTP, which provides both the internal and external interface to Kubernetes. The API server processes and validates REST requests and updates the state of the API objects in etcd, thereby allowing clients to configure workloads and containers across worker nodes.
>  - **Scheduler:** is the extensible component that selects on which node an unscheduled pod (the basic entity managed by the scheduler) runs, based on resource availability.
>  - **Controller:** is a reconciliation loop that drives the actual cluster state toward the desired state, communicating with the API server to create, update, and delete the resources it manages (e.g., pods or service endpoints).
>  - **Controller manager:** is a process that manages a set of core Kubernetes controllers.
>  - **Nodes:** also known as a worker or a minion, is a machine where containers (workloads) are deployed. Every node in the cluster must run a container runtime such as Docker.
>  - **Kubelet:** is responsible for the running state of each node, ensuring that all containers on the node are healthy. It takes care of starting, stopping, and maintaining application containers organized into pods as directed by the control plane.
>  - **Container:** resides inside a pod. The container is the lowest level of a micro-service, which holds the running application, libraries, and their dependencies.
>  - **Pods:** The basic scheduling unit in Kubernetes is a pod, which consists of one or more containers that are guaranteed to be co-located on the same node.
>  - **Services:** is a set of pods that work together, such as one tier of a multi-tier application. The set of pods that constitute a service are defined by a label selector.
>  - **Volumes:** File systems in the Kubernetes container provide ephemeral storage, by default. This means that a restart of the pod will wipe out any data on such containers, and therefore, this form of storage is quite limiting in anything but trivial applications.

<br>

- **Differences between Docker compose and Kubernetes:**

| Docker compose                                                                                                                                                                                                                    | Kubernetes                                                                                                                                                                                                                                                                                                                                                               | 
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Compose is a tool for defining and running multi-container Docker applications.                                                                                                                                                   | Kubernetes is a container orchestrator like Docker Swarm, Mesos Marathon, Amazon ECS, Hashicorp Nomad.                                                                                                                                                                                                                                                                   |
| Manages a group of containers on a single host.                                                                                                                                                                                   | Manages a cluster of hosts running any compatible container runtime.                                                                                                                                                                                                                                                                                                     |
| Not suited to run a high number of containers in the production environment, it can easily overload or fail.                                                                                                                      | Is most suitable for highly loaded production environments with a high number of Docker containers.                                                                                                                                                                                                                                                                      |
| You can manually restart the container on the appropriate machine, but manual management can take a lot of your valuable time and energy.                                                                                         | It has self-healing, restarts containers that fail, replaces containers, kills containers that don't respond to your user-defined health check.                                                                                                                                                                                                                          |
| Compose has commands for managing the whole lifecycle of your application: start, stop, and rebuild services; view the status of running services; stream the log output of running services; run a one-off command on a service. | Container orchestrators are the tools which group hosts together to form a cluster, and help us make sure applications: are fault-tolerant; can scale, and do this on-demand; use resources optimally; can discover other applications automatically, and communicate with each other; are accessible from the external world; can update/rollback without any downtime. |
| Control with `docker-compose`	                                                                                                                                                                                                    | Control with `kubectl`                                                                                                                                                                                                                                                                                                                                                   |

<br>

**4: Possible implementation of Kubernetes in CA4:**

**Wee need to enable Kubernetes in the Docker Desktop app:**
>  - Go to **_Settings_**
>  - **_Kubernetes_**
>  - Tick the **Enable Kubernetes** box
>  - Now that the Kubernetes are activated in the app, we can start using the specific commands: `kubectl` -> https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands

**Since we already did this exercise with Docker compose file, there is a way to convert that file into Kubernetes resources 
by intalling Kompose and using the `kompose convert` command, to convert any docker-compose file.**

**Following the steps in this guide we should be able to convert the docker-compose file from CA4 Part 2 to the CA4 Part 2 
Alternative. (https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/).**

<br>

**5: Conclusion and final commit:**

**As described earlier Docker is a tool for defining and running multi-container applications, wheras Kubernetes is a 
container orchestration engine for automating deployment, scaling, and management of containerized applications.**

**Kubernetes is not a traditional, all-inclusive PaaS (Platform as a Service) system. Since Kubernetes operates at the 
container level rather than at the hardware level, it provides some generally applicable features common to PaaS offerings, 
such as deployment, scaling, load balancing, and lets users integrate their logging, monitoring, and alerting solutions. 
However, Kubernetes is not monolithic, and these default solutions are optional and pluggable. Kubernetes provides the 
building blocks for building developer platforms, but preserves user choice and flexibility where it is important.**

**Both applications have different goals, we can use one or the other, but it seems like we would benefit more to use both, 
from the literature they seem to complement each other quite nicely.**

Since this report was mainly theoretical I based it all from reasearch done in the web, either from https://kubernetes.io/ 
or https://docs.docker.com/, as well as some information gathered in google search, not many findings are original, since 
I'm not that familiar with the Kubernetes platform.
