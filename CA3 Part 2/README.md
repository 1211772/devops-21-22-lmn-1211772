# DEVOPS

## 1. Class Assignment 3

### 1.1 CA3, Part 2 - Virtualization with Vagrant

<br>

**Starting the exercise (CA3 Part 2):**

**Create CA3 Part 2 folder and README.md file**

- **Creating the CA3 Part 2 directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>    - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA3 Part 2\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA3 Part 2 folder and Readme.md (resolves #28)"`**
>  - **`git push`**

<br>

**Doing the online lecture to get to know better Vagrant**

- **Download and install vagrant:**
>  - https://www.vagrantup.com/downloads
>  - open powershell to check if vagrant is installed
>  - **`vagrant -v`** shows the version we are using and therefore if it is installed
>   - notice that vagrant only runs on the host's machine, it can't run inside the VM

- **Create a folder where I want to create the vagrant project as per online lecture:**
>  - first I need to move to the directory I want to create that folder
>  - **`cd D:`**
>  - **`mkdir vagrant-project-1`**
>  - **`cd .\vagrant-project-1\`**

- **Create the vagrant configuration file for the project using a specific box**
>  - **`vagrant init envimation/ubuntu-xenial`**
>  - **`vagrant up`** to initialize the virtualization, you can check your VirtualBox program and see them appear as you execute the command, since VirtualBox is your default
>  - to check the status execute
>   - **`vagrant status`**
>  - you can access the Vm using an SSH session
>   - **`vagrant ssh`**
>  - whenever you want to exit the session just type
>   - **`exit`**
>  - to stop all the VM's first you need to exit the VM, as showed before, then
>   - **`vagrant halt`**

- **Use Vagrant to setup a VM with a Webserver (apache):**
>  - **`cd D:`**
>  - **`cd .\vagrant-project-1\`**
>  - **`git clone https://github.com/atb/vagrant-basic-example.git`**
>  - **`cd .\vagrant-basic-example\`**
>  - **`vagrant up`**
>  - if we access http://localhost:8010/ it shows an example page from apache
>  - **`notepad Vagrantfile`** to change anything you want, in my case I installed nano, so I can access Vagrantfile with it inside the VM
>  - since I made an alteration to the provision section I need to stop the VM and run it again, but doing just vagrant up isn't enough, since the alteration I made isn't one to be executed everytime the VM runs
>  - **`vagrant halt`**
>  - **`vagrant up --provision`** this runs the entire provision section again, making sure that whatever alteration you did in the meantime will execute

<br>

**1: You should use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution**

- **Cloning the repository:**
>  - **`cd D:`**
>  - **`git clone https://1211772@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git`**
>  - delete the .git folder
>   - **`cd .\vagrant-multi-spring-tut-demo\`**
>   - **`rmdir -Force -Recurse "D:\vagrant-multi-spring-tut-demo\.git"`**
>  - copying the Vagrantfile to CA3 Part 2 directory
>   - **`Copy-Item "D:\vagrant-multi-spring-tut-demo\Vagrantfile" -Destination "D:\devops-21-22-lmn-1211772\CA3 Part 2"`**

<br>

**2: Study the Vagrantfile and see how it is used to create and provision 2 VMs**

- **`cd D:\vagrant-multi-spring-tut-demo`**
- **`notepad Vagrantfile`** or open it in IntelliJ
- analyzing the Vagrantfile we can see the vm box configuration which is envimation/ubuntu-xenial
- below that is the provision configuration for both VMs which means both Vms when first booted will install what is inside this provision, if we want something added to both of them it is in this provision we configure it
- after that we have db VM specific configurations such as port configurations, and specific provision for this VM
- next is the web VM specific configurations, ports, memory needed, and specific provision for this VM
- again any configuration we need to make to a specific VM we need to do it in those specific provision sections

<br>

**3: Copy this Vagrantfile to your repository (inside the folder for this assignment)**

- **`Copy-Item "D:\vagrant-multi-spring-tut-demo\Vagrantfile" -Destination "D:\devops-21-22-lmn-1211772\CA3 Part 2"`**

<br>

**4: Update the Vagrantfile configuration so that it uses your own gradle version of the spring application**

- following the steps provided by the teachers we have to change the privacy settings of our repositories, from private to public
- this step is necessary because keeping it private will prompt you to input your repository password which will stop the provision script and therefore breaking it, the script should run without interruptions to execute right, so we change the repository settings to be a public repository so we don't run into this issue

<br>

- **Open your repository:**
>  - bottom left side select
>   - Repository settings
>  - scroll down until:
>   - Access level
>   - tick the box next to "This is a private repository" so it remains clear
>  - bottom right side select
>   - save changes

<br>

- **Go to your Vagrantfile and alter the following information to represent your repository:**
>  - Line 12 change it to : **`sudo apt-get install openjdk-11-jdk-headless -y`**
>  - Line 70 change it to : **`git clone https://1211772@bitbucket.org/1211772/devops-21-22-lmn-1211772.git`**
>  - Line 71 change it to : **`cd devops-21-22-lmn-1211772/'CA2 Part 2'/react-and-spring-data-rest-basic/`**
>  - Line 75 change it to : **`sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar /var/lib/tomcat8/webapps`**

- **Trying to run the application:**
- **`cd D:\devops-21-22-lmn-1211772\CA3 Part 2`**
- **`vagrant up`**

- we come across an error pertaining to JDK 11, with some investigation we discovered that the ubuntu-xenial doesn't have the correct version to run JDK 11
- either we altered the entire application to JDK 8 or we could instead work with the Vagrantfile to find a workaround to this issue
- since the point of the class assignment this week is to work and understand the Vagrantfile, I opted for that route
- I searched for an ubuntu box that had the right version to run JDK 11 and came across ubuntu/bionic64

<br>

- **Changes needed to make it run:**
- **_In the Vagrantfile_**
>  - Line 4 change to : **`config.vm.box = "ubuntu/bionic64"`**
>  - Line 19 change to : **`config.vm.box = "ubuntu/bionic64"`**
>  - Line 46 change to : **`config.vm.box = "ubuntu/bionic64"`**

- It didn't work again, since I copied a Vagrantfile based already on a different box, and ran it already, it remained with that configuration so to remove the box from the local file system type:
- **`vagrant halt`**
- **`vagrant destroy`** needed so that ubuntu/bionic will install instead of envimation/ubuntu-xenial
- **`vagrant box remove envimation/ubuntu-xenial`** to make sure we purge that box from our system

<br>

- **Trying to run the application, again:**
- **`vagrant up`**
- now it works

![](It works.png)

<br>

**5: Check https://bitbucket.org/atb/tut-basic-gradle to see the changes necessary so that the spring application uses the H2 server in the db VM. Replicate the changes in your own version of the spring application.**

- Change files replicating the commits in the repository above-mentioned
- **_In the CA2 Part 2/react-and-spring-data-rest-basic/ build.gradle file_**

```gradle
 plugins {
	id 'org.springframework.boot' version '2.6.6'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
	id "org.siouan.frontend-jdk11" version "6.0.0"
	id 'war'
}

[...]

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-data-rest'
	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	runtimeOnly 'com.h2database:h2'
	testImplementation ('org.springframework.boot:spring-boot-starter-test') {
		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
	}
	// To support war file for deploying to tomcat
	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}

[...]

frontend {
	nodeVersion = "14.17.3"
	assembleScript = "run webpack"
}
```

- **_In the CA2 Part 2/react-and-spring-data-rest-basic/package.json file_**

```json
 "scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
  }
```

- **_In the CA2 Part 2/react-and-spring-data-rest-basic/src/main/java/com/greglturnquist/payroll/ we need to create a new java file:_**

```java
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}
```

- **_In the CA2 Part 2/react-and-spring-data-rest-basic/src/main/resources/application.properties file:_**

```properties
server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

- **_In the CA2 Part 2/react-and-spring-data-rest-basic/src/main/js/app.js file:_**
- Line 17-18 update path

```js
componentDidMount() { // <2>
    client({method: 'GET', path: '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
        this.setState({employees: response.entity._embedded.employees});
    });
}
```

- **_In the CA2 Part 2/react-and-spring-data-rest-basic/src/main/resources/templates/index.html file:_**
- Removed "/" from line 6 after (href="main.css")

```html
<head lang="en">
    <meta charset="UTF-8"/>
    <title>ReactJS + Spring Data REST</title>
    <link rel="stylesheet" href="main.css" />
</head>
```

<br>

- **_In the CA3 Part 2/Vagrantfile file:_**
- Change the previous alteration I made from .war files to .jar files, since I thought that was the reason it didn't work, back to .war
- In line 75
- **`sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps`**

<br>

- **We need to delete .jar files, so when we run the command **`./gradlew clean built`** it creates the intended .war files**
- **`cd D:\devops-21-22-lmn-1211772\CA2 Part 2\react-and-spring-data-rest-basic`**
- **`./gradlew clean build`**

<br>

- **Now we run the application to see if it works**
- **`vagrant up`**
- to see if its working we need to check the following links
- http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT (and as you can see with the images below, on the left side, it works, it shows the information it's supposed to)
- to access the database in the "JDBC URL" field type/paste the following : jdbc:h2:tcp://192.168.56.11:9092/./jpadb
- then click connect, and you should have access to the database console
- http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console (as you can see in the image below, on right side, we accessed the database server, as we should have)
- next I inserted a new entry to my database to see if it worked, and it appeared in the frontend
- **`insert into employee (id, first_name, last_name, description) values ( 2, 'Joao', 'Marques', 'dev')`**


![](It really works.png)

<br>

**6: Describe the process in the readme file for this assignment.**

- Since I already described the process along the way of making this file, I will only talk a bit about what I learned from vagrant and my experience with it, as well as the final commit for this class assignment
- The difference from the CA3 Part 1 to this one was abismal, it took way less steps to prepare because we only created a file that runs all the configuration steps in a single command, instead of inputting them one by one
- It was a bit tricky to adapt at first, but once I understood the logic of the Vagrantfile I think it was more enjoyable and better to work with instead of the CA3 Part 1 way

<br>

**7: At the end of the part 2 of this assignment mark your repository with the tag ca3-part2.**

- **_Commit the changes and push_**
- **`cd D:\devops-21-22-lmn-1211772`**
- **`git add .`**
- **`git commit -m "Update Readme.md and CA3 Part 2 (resolves #31)"`**
- **`git push`**
- **`git tag ca3-part2`**
- **`git push origin ca3-part2`**

