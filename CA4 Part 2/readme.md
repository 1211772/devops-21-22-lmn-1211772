# DEVOPS

## 1. Class Assignment 4

### 1.1 CA4, Part 2 - Containers with Docker

<br>

**Starting the exercise (CA4 Part 2):**

**Create CA4 Part 2 folder and README.md file**

- **Creating the CA4 Part 2 directory:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>  - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA4 Part 2' -ItemType Directory`**

- **Cloning the example repository provided by the professor to the just created directory:**
>  - **`cd 'D:\devops-21-22-lmn-1211772\CA4 Part 2'`**
>  - **`git clone https://1211772@bitbucket.org/atb/docker-compose-spring-tut-demo.git`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA4 Part 2 folder and docker files and docker-compose (resolves #42)"`**
>  - **`git push`**

<br>

**The goal of this assignment is to use Docker to setup a containerized environment to execute your version of the gradle 
version of the spring basic tutorial application.**

**1: You should produce a solution similar to the one of the part 2 of the previous CA but now using Docker instead of Vagrant:**

- **_Since in the previous class assignment I altered the CA2 Part 2, instead of copying it to the CA3 Part 2 folder, 
I'm going to use the CA2 Part 2 folder._**

<br>

**2: You should use docker-compose to produce 2 services/containers:**

- **The <ins>_web_</ins> container  will be used to run Tomcat and the spring application**
- **The <ins>_db_</ins> container  will be used to execute the H2 server database**

<br>

- **Creating the Dockerfiles**
>- _Since I cloned the repository from the lecture I'm just going to alter what I need to accommodate my needs_

<br>

- **Altering the <ins>_db_</ins> Dockerfile**

```Dockerfile
FROM ubuntu:18.04

RUN apt-get update && \
  apt-get install -y openjdk-11-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
```

<br>

- **Altering the <ins>_web_</ins> Dockerfile**

```Dockerfile
FROM tomcat:9.0-jdk11-temurin

RUN apt-get update -y 

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://1211772@bitbucket.org/1211772/devops-21-22-lmn-1211772.git

WORKDIR /tmp/build/devops-21-22-lmn-1211772/'CA2 Part 2'/react-and-spring-data-rest-basic/

RUN chmod u+x gradlew

RUN ./gradlew clean build && cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ && rm -Rf /tmp/build/

EXPOSE 8080
```

<br>

- **Altering the docker-compose.yml**

```yml
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.142.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.142.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.142.0/24
```

<br>

- **_I had an issue here regarding the ip, it wasn't accepting the one I inserted, it was using a default, so I altered every
ip to stay in the same network, took down all the containers and images and build everything from scratch, and it worked._**

<br>

- **_Also this alteration meant I had to alter the application.properties from CA2 Part 2:_**
>- _Line 5 in application.properties_
>- `spring.datasource.url=jdbc:h2:tcp://192.168.142.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE`

<br>

- **Building the images**
>- **`cd 'D:\devops-21-22-lmn-1211772\CA4 Part 2'`**
>- **`docker-compose up`**

<br>

- **Checking if everything is working properly**
>- In the host machine open a web browser to check the <ins>_web_</ins> application
>- **`http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/`**
>- To check the <ins>_db_</ins> application
>- **`http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`**
>- To connect to the use the following input:
>- **`jdbc:h2:tcp://192.168.142.11:9092/./jpadb`**
>- To check if the database and web are connected add something to the table employee:
>- **`into employee (id, first_name, last_name, description) values ( 2, 'Gandalf', 'The White', 'Sorcerer')`**
>- Press `Run` and update the webpage for the web application to see if it worked.

<br>

- **Check the images bellow to confirm that it worked**

![](ca4part2.PNG)

<br>

**3: Publish the images (db and web) to Docker Hub (https://hub.docker.com):**

>- **_In Powershell:_**
>  - in the CA4 Part 2 folder
>  - **`docker login`**


>  - Publishing the <ins>_web_</ins> image
>  - **`docker tag ca4part2_web 1211772/ca4part2_web:1.0`** (docker tag image user/image:tag_version)
>  - **`docker push 1211772/ca4part2_web:1.0`** (docker push user/image:tag_version)


>  - Publishing the <ins>_db_</ins> image
>  - **`docker tag ca4part2_db 1211772/ca4part2_db:1.0`** (docker tag image user/image:tag_version)
>  - **`docker push 1211772/ca4part2_db:1.0`** (docker push user/image:tag_version)

![](docker-hub.PNG)

<br>

**4: Use a volume with the db container to get a copy of the database file by using the exec to run a shell in the 
container and copying the database file to the volume:**

>- **_List the containers:_**
>  - **`docker ps`**
>  - <ins>_db_</ins> container id - 25913d5b4ef8


>- **_Enter the container:_**
>  - **`docker-compose exec db bash`**


>- **_Copy the database file:_**
>  - **`cp jpadb.mv.db /usr/src/data-backup`**


>- **_Leave the container:_**
>  - **`exit`**

<br>

**5: Describe the process in the readme file for this assignment. Include all the Docker files in your repository 
(i.e., docker-compose.yml, Dockerfile):**

>- **_Commit the changes and push:_**
>  - **`git add .`**
>  - **`git commit -m "Updated readme and added images to it (resolves #44)"`**
>  - **`git push`**

<br>

**6: At the end of this assignment mark your repository with the tag ca4-part2:**

>- **_Commit the changes and push:_**
>  - **`git add .`**
>  - **`git commit -m "Final changes to readme file (resolves #45)"`**
>  - **`git push`**
>  - **`git tag ca4-part2`**
>  - **`git push origin ca4-part2`**