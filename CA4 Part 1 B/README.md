# DEVOPS

## 1. Class Assignment 4

### 1.1 CA4, Part 1 - Containers with Docker

<br>

**1: Starting the exercise (CA4 Part 1 B):**

**Create CA4 Part 1 B folder and README.md file**

- **Creating the CA4 Part 1 B directory, README.md:**
>- **_Firstly open the PowerShell_**:
   >  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>  - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA4 Part 1 B\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "created folders and readme's for ca4 part 1 (resolves #35)"`**
>  - **`git push`**

<br>

**2: Set up a Dockerfile:**

- **_In this exercise it's supposed to build the chat server in your host computer and copy the .jar file "into" the Dockerfile_**

>- **_Creating a Dockerfile:_**
>  - In the CA4 Part 1 B folder in IntelliJ
>  - Create a file named Dockerfile
>  - Start writting what you want in the file (**FROM, RUN, WORKDIR, EXPOSE, CMD**)

```Dockerfile
# Starting with base image
FROM ubuntu:18.04

# Install dependencies
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

# Copy the jar file
COPY gradle_basic_demo/build/libs/basic_demo-0.1.0.jar .

# Expose port
EXPOSE 59001

# Execute the container
CMD java -cp  basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

>- **_Copy the application https://bitbucket.org/luisnogueira/gradle_basic_demo/ into CA4 Part 1 B:_**
>  - **`git clone https://1211772@bitbucket.org/luisnogueira/gradle_basic_demo.git`**
>  - Delete the .git folder when copying the aplication into the repository! (it's a hidden file)
>  - **`cd D:\devops-21-22-lmn-1211772\'CA4 Part 1 B'\gradle_basic_demo`**
>  - **`./gradlew clean build`**

<br>

**3: Creating an image:**

>- **_In Powershell:_**
>  - **`cd D:\devops-21-22-lmn-1211772\'CA4 Part 1 B'\`**
>  - **`docker build -t ca4part1b .`**

<br>

**4: Creating and running a container:**

>- **_In Powershell:_**
>  - **`docker run --name container_2 -p 59001:59001 -d ca4part1b`**
>  - the command --name lets me choose the name I want for my container by typing it next to the command
>  - the command -d lets me choose the name I want for my image by typing it next to the command

<br>

**5: Tagging the image and publishing it on Docker Hub:**

>- **_In Powershell:_**
>  - in the CA4 Part 1 B folder
>  - **`docker login`**
>  - **`docker tag ca4part1b 1211772/ca4part1b:1.0`** (docker tag image user/image:tag_version)
>  - **`docker push 1211772/ca4part1b:1.0`** (docker push user/image:tag_version)

![](push to repo b.PNG)

<br>

**6: Executing the chat client on the host machine:**

>- **_While running the container with the server running:_**
>  - in another Powershell go to `cd D:\devops-21-22-lmn-1211772\'CA4 Part 1 B'\gradle_basic_demo` (wich is a clone of the professors' repository -> https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/)
>  - **`./gradlew runClient`**
>  - the chat application is working as intended

<br>

**7: Committing everything to the repository and finalizing CA4 Part 1 B:**

>  - **`git add .`**
>  - **`git commit -m "committing files and updating readme for part b (resolves #41)"`**
>  - **`git push`**
>  - **`git tag ca4-part1b`**
>  - **`git push origin ca4-part1b`**