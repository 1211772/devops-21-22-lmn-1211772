##Alternative to Gradle

###Gradle

Gradle is a build automation tool, with the advantage of it being open-source, and also being very flexible in which it
can build almost any type of software. Some key features are its high performance, since, by only running tasks that it
needs to run (either because their inputs or the outputs have changed), avoid unnecessary work, and in doing so unnecessary
restrain the machine; it implements conventions, same as Maven, which means that, by applying the appropriate plugins, you
can build shorter scripts for many projects, making it also easier to build; and its extensibility, you can create your own
task types and build model.

Gradle is written in Groovy, and as mentioned earlier it is used by almost any type of software, which means it supports 
several programming languages, such as: Java, Scala, Android, C/C++ and Groovy.

**_Advantages of using Gradle:_**
- It’s an expressive, declarative, and maintainable build tool;
- Supports dependency management;
- Provides very scalable and high-performance builds;
- Provides standard project layout and lifecycle, but it’s fully flexible;
- It’s very easy to use and implement custom logic in our project;
- Supports the project structure that consists of more than one project to build deliverable;
- It’s very easy to integrate existing Ant/Maven project with Gradle;
- It’s very easy to migrate from existing Ant/Maven project to Gradle;
- It has great performance, it does a stupendous job in speeding up your builds with features like incremental builds, daemons and clustered builds.

**_Disadvantages of using Gradle:_**
- Technical Expertise: To build tasks with Gradle, technical skills are needed;
- Understandability: Gradle documentation is quite extensive. It needs the knowledge of terms in prior;
- Gradle's dependency cache is not portable. If you copy it between machines, it will break in most cases;
- Per dependency repository configuration is not possible;
- It’s flexibility might also be a disadvantage, since you cna have hard to understand build scripts because of it.

<br>

****

**From the search I made there are a few ways I can implement an alternative to gradle, to name a few:**
- CMake;
- GNU Make;
- SCons;
- Meson;
- Buck;
- Rake;
- Ant;
- Maven;

**Since I am already familiar with Maven I chose that way to implement the alternative.**

###Maven

Maven is a popular open-source build tool that automates the entire process of building, publishing, and deploying several projects, 
it is based on Project Object Model (POM) and its focus is on simplification and standardization of the building process.
Maven makes the day-to-day work of Java developers easier and generally help with the comprehension of any Java-based project.

Maven is written in Java, and it supports several programming languages, such as: C#, Scala, Ruby, etc.

**_Advantages of using Gradle:_**
- It’s also an expressive, declarative, and maintainable build tool;
- Also supports dependency management;
- All maven projects follow pre-defined structure;
- Provides a very modularized project structure;
- Model-based builds- Maven can develop different projects into predefined output types like war, metadata, and jar;
- Backward Compatibility − One can readily port the different modules of a project into the latest version of Maven from the older versions. It also supports the older versions;
- Automatic parent versioning − There is no requirement to designate the parent in the sub-module for maintenance;
- Parallel builds − It examines the project dependency graph and lets you design schedule modules in parallel. With the help of this, you can obtain performance improvements of 20-50%.

**_Disadvantages of using Gradle:_**
- Follows some pre-defined build and automation lifecycle;
- Writing custom maven life cycle methods is a bit tough and verbose;
- Build scripts tougher to maintain for very complex projects;
- Per dependency repository configuration is not possible;
- It’s flexibility might also be a disadvantage, since you cna have hard to understand build scripts because of it.

<br>

****

###Biggest differences between Gradle and Maven

| Gradle                                                                         | Maven | 
|--------------------------------------------------------------------------------|---------------------------------------------|
| As Groovy is dynamically typed, it’s really hard for IDEs to provide good and fast (!) tooling                             | Contrarily, parsing and interpreting Maven’s XML is dead simple
| Provides standard project layout and lifecycle, but it’s fully flexible, giving the option to fully configure the defaults | Follows some pre-defined build and automation lifecycle
| It uses a Groovy-based Domain-specific language (DSL) for creating project structure                                       | It uses Extensible Markup Language (XML) for creating project structure
| Developing applications by adding new features to them                                                                     | Developing applications in a given time limit
| It performs better than maven as it is optimized for tracking only current running task                                    | It does not create local temporary files during software creation, and is hence – slower
| It avoids compilation 	                                                                                                 | It is necessary to compile
| This tool is highly customizable as it supports a variety of IDE’s 	                                                     | This tool serves a limited amount of developers and is not that customizable

<br>

****

###Implementing the alternative

<br>

**1: Create CA2 Part 2 Alternative folder and README.md file, still in the branch tut-basic-gradle**

**Creating the CA2 Part 2 directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>    - `cd D:\devops-21-22-lmn-1211772\`

**_Creating the directory and the file:_**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\README.md' -ItemType File -Force`**

<br>

**2: Start a new maven spring boot project following the link provided**

<br>

**3: Copy and extract the generated zip file inside the directory CA2 Part 2**

- **_Copying the zip file to the CA2 Part 2 Alternative folder:_**
>  - **`Copy-Item "C:\Users\Marques\Downloads\react-and-spring-data-rest-basic.zip" -Destination "D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative"`**

- **_Extract the zip file into CA2 Part 2 Alternative folder:_**
>  - **`Expand-Archive -Path react-and-spring-data-rest-basic.zip -DestinationPath "D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative"`**

<br>

**4: Deleting the src folder inside react-and-spring-data-rest-basic**

**Deleting the src folder**
>- **_Then delete the aforementioned folder:_**
   >  - **`rmdir -Force -Recurse "D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\react-and-spring-data-rest-basic\src"`**

<br>

**5: Copy the src folder (and all its subfolders) from the basic folder of the tutorial into this new folder.**

**Copying the folder src from CA1\tut-react-and-spring-data-rest\basic\ to CA2 Part 2\react-and-spring-data-rest-basic**
>- **_To do so use the following command:_**
>  - Copy the folder:
>    - **`Copy-Item -Path "D:\tut-react-and-spring-data-rest\basic\src" -Destination "D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\react-and-spring-data-rest-basic" -recurse -Force`**

**Copying the webpack.config.js and package.json**
>- **_To do so use the following command:_**
>  - Copy the webpack.config.js file:
>    - **`Copy-Item "D:\tut-react-and-spring-data-rest\basic\webpack.config.js" -Destination "D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\react-and-spring-data-rest-basic"`**
>  - Copy the package.json file:
>    - **`Copy-Item "D:\tut-react-and-spring-data-rest\basic\package.json" -Destination "D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\react-and-spring-data-rest-basic"`**

**Deleting the src/main/resources/static/built/**
>- **_Delete the aforementioned folder:_**
   >  - **`rmdir -Force -Recurse "D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\react-and-spring-data-rest-basic\src\main\resources\static\built"`**

<br>

**6: Experiment with the application**

**Run the application**
>- **`./mvnw spring-boot:run`**
   >   - The web page http://localhost:8080 is empty as it should.

<br>

**7 & 8: Add the maven plugin org.siouan.frontend**
>- **followed the steps in the README.md file in the link provided -> https://github.com/eirslett/frontend-maven-plugin**

<br>

**9: Add also the following code in build.gradle to configure the previous plugin**

```pom.xml
 <plugin>
	<groupId>com.github.eirslett</groupId>
	<artifactId>frontend-maven-plugin</artifactId>
	<version>1.12.1</version>
	<executions>

		<execution>
			<id>install node and npm</id>
			<goals>
				<goal>install-node-and-npm</goal>
			</goals>
			<configuration>
				<!-- See https://nodejs.org/en/download/ for latest node and npm (lts) versions -->
				<nodeVersion>v16.14.2</nodeVersion>
				<npmVersion>8.5.0</npmVersion>
			</configuration>
		</execution>

		<execution>
			<id>npm install</id>
			<goals>
				<goal>npm</goal>
			</goals>
				<!-- Optional configuration which provides for running any npm command -->
			<configuration>
				<arguments>install</arguments>
			</configuration>
		</execution>

		<execution>
			<id>npm run build</id>
			<goals>
				<goal>npm</goal>
			</goals>
			<configuration>
				<arguments>run build</arguments>
			</configuration>
		</execution>

	</executions>
</plugin>
```

<br>

**10: Update the scripts section/object in package.json to configure the execution of webpack**

```json
 "scripts": {
	"webpack": "webpack",
	"build": "npm run webpack",
	"check": "echo Checking frontend",
	"clean": "echo Cleaning frontend",
	"lint": "echo Linting frontend",
	"test": "echo Testing frontend"
},
```

<br>

**11: Executing the build of the application to see that it runs, the tasks related to the frontend are also executed and the frontend code is generated**

**`./mvnw install`**

<br>

**12: Executing the application**

**Run the application**
>- **`./mvnw spring-boot:run`**
   >   - The web page http://localhost:8080 is now showing the information.

<br>

**13: Add a task to pom.xml to copy the generated jar to a folder named "dist" located at
the project root folder level**

**Add the task copy jar**
>- **Create the folder dist**
   >   - **`mkdir dist`**

**Create the task**

```pom.xml
 <executions>
	<execution>
		<id>copy-resource-one</id>
		<phase>generate-sources</phase>
		<goals>
			<goal>copy-resources</goal>
		</goals>
		<configuration>
			<outputDirectory>dist</outputDirectory>
			<resources>
				<resource>
					<directory>target</directory>
					<includes>
						<include>**/*.jar</include>
					</includes>
				</resource>
			</resources>
		</configuration>
	</execution>
</executions>
```

<br>

**14: Add a task to gradle to delete all the files generated by webpack**

**Create the task delete built folder**

```pom.xml
 <plugin>
	<artifactId>maven-clean-plugin</artifactId>
	<version>3.2.0</version>
	<configuration>
		<filesets>
			<fileset>
				<directory>src/main/resources/static/built</directory>
				<followSymlinks>false</followSymlinks>
			</fileset>
		</filesets>
	</configuration>
 </plugin>
```

<br>

**15: Experiment all the developed features**

**Building the application**

**It can either be `./mvnw spring-boot:run` or `./mvnw install`**

>- **_After running the application you can see that the task copy-resources was done successfully:_**
   >    - After doing this you can see that the .jar files in /target were copied with success to the dist folder


```./mvnw spring-boot:run
[INFO] --- maven-resources-plugin:3.2.0:copy-resources (copy-resource-one) @ alternative ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Using 'UTF-8' encoding to copy filtered properties files.
[INFO] Copying 1 resource
```

>- **_Check if the task delete is working:_**
   >    - After doing this you can see that the task deleted the folders

```./mvnw clean
[INFO] --- maven-clean-plugin:3.2.0:clean (default-clean) @ alternative ---
[INFO] Deleting D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\react-and-spring-data-rest-basic\target
[INFO] Deleting D:\devops-21-22-lmn-1211772\CA2 Part 2 Alternative\react-and-spring-data-rest-basic\src\main\resources\static\built (includes = [], excludes = [])
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
```

<br>

**15/16/17: Committing changes, merging with the master branch and tag the end of the class assignment with ca2-part2**

<br>

****

###Conclusion

I found working with Gradle enjoyable, much more than Maven, partly because it´s easier to understand, logically, not
as confusing as reading a pom.xml. I also liked the flexibility of writing our own tasks and executing only said task to 
see if it´s running. The easy of the dependency we can introduce between tasks. The commands at our disposal in Gradle are 
very useful too. Maven I knew, but since I wasn't very familiar with all the process, researching and troubleshooting was 
harder to understand than Gradle, maybe because of the ease of readability of Gradle and the more logic based language.
The speed in which the application ran in Gradle was astonishing compared to Maven.
Overall I preferred working with gradle over Maven, even though there are very similarities between both.
