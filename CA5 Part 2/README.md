# DEVOPS

## 1. Class Assignment 5

### 1.1 CA5, Part 2 - CI/CD Pipelines with Jenkins

# Disclaimer

### When trying to implement the alternative (CA5 Part 2 Alternative), I ended up messing the CA2 Part 1 folder used in this assingment, the assingmet was already done, as you can see from the photos posted in this readme.

<br>

**Starting the exercise (CA5 Part 2):**

**Create CA5 Part 2 folder and README.md file**

- **Creating the CA5 Part 2 directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>    - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA5 Part 2\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA5 Part 2 folder and Readme file (resolves #51)"`**
>  - **`git push`**

<br>

**1: The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the
tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).**

**I tried using CA2 Part 2, but I couldn't run tests, and had some other errors, since CA2 Part 1 
was functioning the professor agreed that I could do CA5 Part 2 assignment with the CA2 Part 1.**

- **Setting up Jenkins:**
>  - **Download jenkins.war from -> https://www.jenkins.io/download/**
>  - **Open PowerShell in the folder that you saved the downloaded file**
>  - **Execute `java -jar jenkins.war`** (this command by deafult uses the port :8080, because, 
     from previous Class Assingments, this port is already taken, we have to execute a 
     different command to force the jenkins to run in a specific port)
>  - **Execute `java -jar jenkins.war --httpPort=9090`**

- **Setting a new pipeline job:**
>  - **Open `http://localhost:9090` to access Jenkins**
>  - **Go to `Dashboard`**
>  - **Select `New Item`**
>  - **Naming the job `Name:`** (I named it "ca5_part2")
>  - **Click on `Pipeline` and then `OK`**

- **To create the pipeline from a Jenkinsfile, follow these steps:**
>  - **Click on `Pipeline`**
>  - **In `Definition`**
>  - **Choose the option `Pipeline script from SCM`**
>  - **In defining the `Source Control Manager` choose `Git`**
>  - **Paste your repository URL: https://bitbucket.org/1211772/devops-21-22-lmn-1211772**
>  - **Since our main branch isn't the default `*/master`, specify which branch is your main one `*/main`**
>  - **Specify the script path: `CA2 Part 1/gradle_basic_demo/Jenkinsfile`**

- **Aditional configurations to Jenkins to acomodate the new assignment:**
>  - **Open `http://localhost:9090` to access Jenkins**
>  - **Go to `Manage Jenkins`**
>  - **Press `Manage Plugins`**
>  - **Select the tab `Available`**
>  - **In the search bar search `HTML Publisher`**
>  - **Tick the box to install the plugin `HTML Publisher`**
>  - **In the search bar search `Docker Pipeline`**
>  - **Tick the box to install the plugin `Docker Pipeline`**
>  - **Restart the jenkins**

- **Now to manage the credentials for the acces to the Docker Hub:**
>  - **Open `http://localhost:9090` to access Jenkins**
>  - **Go to `Manage Jenkins`**
>  - **Press `Manage Credentials`**
>  - **Click on `Jenkins` inside the table `Credentials`**
>  - **Click on `Global credentials` inside the table `System`**
>  - **Press the button `Add credentials` on the left side**
>  - **Kind:** Username with password
>  - **Scope:** Global
>  - **Username:** same as the Docker Hub
>  - **Password:** same as the Docker Hub
>  - **ID:** dockerhub_credentials
>  - **Description:** (you can leave it blank)
>  - **Press `OK`**

<br>

**2: You should define the following stages in your pipeline**

- **Checkout.** To checkout the code form the repository
- **Assemble.** Compiles and Produces the archive files with the application. Do not use the build 
task of gradle (because it also executes the tests)!
- **Test.** Executes the Unit Tests and publish in Jenkins the Test results. See the junit step 
for further information on how to archive/publish test results.
- Do not forget to add some unit tests to the project (maybe you already have done it).
- **Javadoc.** Generates the javadoc of the project and publish it in Jenkins. See the publishHTML step 
for further information on how to archive/publish html reports.
- **Archive.** Archives in Jenkins the archive files (generated during Assemble).
- **Publish Image.** Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.
- See https://jenkins.io/doc/book/pipeline/docker/, section Building Containers.
- See also the next slide for an example. In the example, docker.build will build an image from a Dockerfile 
in the same folder as the Jenkinsfile. The tag for the image will be the job build number of Jenkins.

<br>

- **Since I already aforementioned I am using the CA2 Part 1 Jenkinsfile, so instead I'm going to edit it to
acomodate the changes needed for this assingment:**
>  - **In Intellij**
>  - **In `CA2 Part 1/gradle_basic_demo/`**
>  - **Open file `Jenkinsfile`**

<br>

- **After editing it the Jenkinsfile should look like this:**

```Jenkinsfile
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'main', url: 'https://bitbucket.org/1211772/devops-21-22-lmn-1211772'
            }
        }

        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    bat './gradlew clean assemble'
                    }
            }
        }

        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    bat './gradlew test'
                    }
            }
        }

        stage('Jar') {
            steps {
                echo 'Generating Jar...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    bat './gradlew jar'
                    }
            }
        }

        stage('Javadoc') {
            steps {
                echo 'Creating Javadoc...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    bat './gradlew javadoc'
                    }
            }
        }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*'
                    }
            }
        }

        stage ('Docker Image') {
            steps {
                dir('CA2 Part 1/gradle_basic_demo') {
                    script {
                        docker.withRegistry('https://registry.hub.docker.com', 'dockerhub_credentials') {
                            def customImage = docker.build("1211772/ca5_part2:${env.BUILD_ID}")
                            customImage.push()
                        }
                    }
                }
            }
        }

    }

    post {
        always {
            junit 'CA2 Part 1/gradle_basic_demo/build/test-results/test/*.xml'
            publishHTML([allowMissing: false,
            alwaysLinkToLastBuild: false,
            keepAll: false, reportDir: 'CA2 Part 1/gradle_basic_demo/build/docs/javadoc',
            reportFiles: 'index.html',
            reportName: 'HTML Report',
            reportTitles: 'The Report'])
        }
    }
}
```

<br>

- **Create a Dockerfile, or use the Dockefile from the CA4 Part 1 assignment:**

```Dockerfile
# Download base image ubuntu 18.04
FROM ubuntu:18.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Install java
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

# Copy the jar file into the container
COPY build/libs/basic_demo-0.1.0.jar .

# Expose Port for the Application
EXPOSE 59001
# Run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

<br>

**3: Excuting the build and see the results in Jenkins**

- **Open `http://localhost:9090` to access Jenkins**
- **Choose the Job `ca5_part2`**
- **Click on `Build Now`**
- **I had severall errors ocurring during these builds, I tried it in my laptop and the build was failing 
in the Dockerfile line where it needs to install the jdk 11.**
- **From what I gathered the error might have ocurred, either from an internet issue, or from a cache problem, 
if it's the latter try running `docker builder prune -a`, if it's the former try again with a stabler internet 
connection.**
- **I didn't follow those steps though, I tried it in my desktop, from scratch, and it worked as you can see 
from the image bellow.**
- **Those two builds that failed was because, since I did everything from scratch, I forgot to install the plugins 
in Jenkins, and the other one was because I forgot the credential for the Docker Hub.**

![](jenkins.png)

![](docker hub.PNG)

<br>

**4: Final commit with changes to readme:**

>- **_Commit the changes and push:_**
>  - **`git add .`**
>  - **`git commit -m "Final changes to readme file (resolves #54)"`**
>  - **`git push`**
>  - **`git tag ca5-part2`**
>  - **`git push origin ca5-part2`**