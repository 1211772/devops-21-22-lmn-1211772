# DEVOPS

## 1. Class Assignment 3

### 1.1 CA3, Part 1 - Virtualization with Vagrant

<br>

**Starting the exercise (CA3 Part 1):**

**1: Create CA3 Part 1 folder and README.md file**

- **Creating the CA3 Part 1 directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>    - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA3 Part 1\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA3 Part 1 folder and Readme.md (resolves #25)"`**
>  - **`git push`**

<br>

- **Install Virtual Box as described in lecture:**
>  - go to https://www.virtualbox.org/wiki/Downloads
>  - select VirtualBox 6.1.34 platform package Windows Hosts

- **Create new Virtual Machine (VM):**
>  - open the just installed VirtualBox
>  - select "New"
>  - choose a name for your VM (in my case it was just ubuntu, as I did this step at the same time as the online lecture I mimicked the name of the professor VM)
>  - in the memory RAM selection I selected 2048 Mb as per the instructions of the professor
>  - press the option "Create a virtual hard disk now"
>  - select the option "VDI (VirtualBox Disk Image)"
>  - next choose the "Dynamically allocated" option
>  - finally, click "Create"

- **Edit VM settings:**
>  - select your just created VM
>  - press "Settings"
>  - then "Storage"
>  - click "Empty"
>  - select "Optical Drive"
>   - download the ubuntu .iso image form this site https://help.ubuntu.com/community/Installation/MinimalCD
>   - choose the 64-bit PC (amd64, x86_64)
>   - this will download mini.iso which contains the bare minimal image to install the ubuntu OS with just the terminal
>   - select the mini.iso file
>  - check the "Live CD/DVD" box

- **Installing Ubuntu:**
>  - select your VM
>  - press "Start"
>  - installing Ubuntu following the configurations steps per instructed in the lecture
>  - afterwards it is important to remove the image we mounted in the drive disk, or else it will always boot the system to install the Ubuntu system again
>   - for that go again to "Settings"
>   - then "Storage"
>   - remove the mini.iso file from the "Optical Drive" by pressing "Remove Disk from Virtual Device"
>  - then start your VM

- **Create a host-only network:**
>  - turn off your VM
>  - select "File"
>  - then "Host Network Manager"
>  - since a Host-only adapter was already created, instead of creating a new one as per the lecture I just selected that one
>  - go to your VM again
>  - press "Settings"
>  - then "Network"
>  - select "Adapter 2"
>  - tick the box "Enable Network Adapter"
>  - in the "Attached to" selection change it to "Host-only Adapter"
>  - select the only adapter that appears since we only have that one that was already created

<br>

- **Configuring the VM:**
>  - start your VM
>  - in the terminal of the VM
>   - **`sudo apt update`**
>   - **`sudo apt install net-tools`**

- **_Edit the network configuration file to setup the IP_**
>   - **`sudo nano /etc/netplan/01-netcfg.yaml`**

- **_The file should look like this:_**

```nano
 - network:
        version: 2
        renderer: networkd
        ethernets:
          enp0s3:
            dhcp4: yes
          enp0s8:
            addresses:
              - 192.168.56.5/24
```

>  - **`ctrl+o`** to save
>  - **`ctrl+x`** to exit nano

- **_Applying the just altered network changes:_**
>  - still in the VM terminal
>   - **`sudo netplan apply`**

- **_Install openssh-server:_**
>  - **`sudo apt install openssh-server`**

> - **_Enable password authentication for ssh:_**
>  - **`sudo nano /etc/ssh/sshd_config`**
>  - uncomment the line PasswordAuthentication yes (ctrl+o to save and ctrl+x to exit)
>  - **`sudo service ssh restart`**

> - **_Install an ftp server:_**
>  - **`sudo apt install vsftpd`**

> - **_Enable write access for vsftpd:_**
>  - **`sudo nano /etc/vsftpd.conf`**
>  - uncomment the line write_enable=YES (ctrl+o to save and ctrl+x to exit)
>  - **`sudo service vsftpd restart`**

> - **_Since the SSH server is enabled in the VM we can now use ssh to connect to the VM:_**
>  - open powershell
>  - **`ssh marques@192.168.56.5`** (it's the ip we configured earlier in the network configuration file)

> - **_Installing git in the VM:_**
>  - **`sudo apt install git`**

> - **_Installing JDK 11:_**
>  - **`sudo apt install openjdk-11-jdk-headless`**

<br>

**2: Clone your individual repository inside the VM**

- **`git clone https://1211772@bitbucket.org/1211772/devops-21-22-lmn-1211772.git`**

<br>

**3 & 4: Trying to build and execute the CA1 application**

- **_Run the CA1 application:_**
>  - **`cd devops-21-22-lmn-1211772`**
>  - **`cd CA1/`**
>  - **`cd tut-react-and-spring-data-rest/`**
>  - **`cd basic/`**
>  - **`./mvnw spring-boot:run`**
>   - I couldn't run the application, after investigating I discovered I needed to give permission to the Vm to execute the command
>  - **`chmod u+x mvnw`**
>  - **`./mvnw spring-boot:run`** again
>  - In the hosts browser we can see that the application in running as intended (http://192.168.56.5:8080/)

<br>

**3 & 5: Trying to build and execute the CA2 Part 1 application**

- **_Run the CA2 Part 1 application:_**
>  - To not make the same mistake as before I'll give gradlew permission to execute first
>  - **`chmod u+x gradlew`**
>  - **`cd 'CA2 Part 1/'`**
>  - **`cd gradle_basic_demo/`**
>  - **`./gradlew build`**
>  - **`./gradlew runServer`**

- In the hosts machine it was supposed to work as intended, but since I was running the server in the VM, I remembered I had to alter the ip that runs it in the CA2 Part1 build.gradle, from 'localhost' to the VM's ip (192.168.56.5), as shown next in the args line.

```gradle
 task runClient(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59001 "
  
    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}
```

- After this alteration when I run the server inside the VM and execute the chat application (./gradlew runClient) in the host computer it works as intended.

<br>

**3 & 4: Trying to build and execute the CA2 Part 2 application**

- **_Run the CA2 Part 2 application:_**
>  - Give gradlew permission to execute first
>  - **`chmod u+x gradlew`**
>  - **`cd 'CA2 Part 2/'`**
>  - **`cd react-and-spring-data-rest-basic/`**
>  - **`./gradlew build`**
>  - **`./gradlew bootRun`**
>  - open browser in host machine http://192.168.56.5:8080/, and it works as intended, we can see the frontend of the app.
