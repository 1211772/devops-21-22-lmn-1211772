# DEVOPS

## 1. Class Assignment 1

### 1.1 CA1 Version Control with Git
### 1.1.1 First week no branches

<br>

**Part 1**

**Git commands:**

> **Initial Git Setup:**
> - Displaying the git configuration:
>  - **git config -l**
> - Setting up your identification:
>  - **git config --global user.name "Joao Marques [1211772]"**
>  - **git config --global user.email 1211772@isep.ipp.pt**
> - Setting up your text editing tool:
>  - **git config --global core.editor nano**

<br>

**Starting the exercise (CA1):**

**1: Copy the code of the Tutorial React.js and Spring Data REST Application into a
new folder named CA1**

- **Creating the CA1 directory, README.md and copying the aforementioned repository into this:**
>- Firstly open the program git bash inside the local repository:
   >  - In my case it's here (d/devops-21-22-lmn-1211772)
>
- **There is a few ways we can do this step:**
>- **_First way:_**
>  - Creating the directory inside d/devops-21-22-lmn-1211772:
>    - **mkdir CA1**
>  - Move inside the directory
>    - **cd CA1/**
>  - Creating README inside the directory
>    - **touch README.md**

>- **_Second way is much faster, and you don't have to move from d/devops-21-22-lmn-1211772:_**
> - Creating the directory and the file:
>    - **mkdir CA1 && touch CA1/README.md**
> - Copying the aforementioned repository into CA1:
>    - **cp -R tut-react-and-spring-data-rest CA1/**

<br>

**2 & 3: Commit the changes (and push them)**


- **Committing the changes and add tags to identify the versions of the commits as well as using issues to identifying them with messages associated to the commits:**
    - To use issues and identify them in the messages you have to create them first in the bitbucket repository, in the tab issues.
    - In my case I created 3 separate issues addressing the creation of the folder, the file, and copying the repository to the just created directory.

>- **Committing the changes:**
>  - Adding all the changes made to local repository in queue to commit to remote repository, adding them to the staging area:
>    - **git add .**
>  - Committing the changes in the staging area, you can also add a message with your commit, it can be done in a number of ways:
>    - **git commit**
>      - opens the default text editor to add a message, in this case is nano, as we configured earlier
>    - You can do this in a single command
>      - **git commit -m "description (addresses or resolves (number of issue) #1)"**
>  - Normally before the push you should pull changes from the remote repository (git pull), but since we are working alone we don't need to.
>  - Pushing the changes added to the staging area to the remote repository:
>    - **git push**
>  - **Adding a tag to the commit:**
>    - You can either add tag without any annotation associated:
>      - **git tag v1.1.0**
>    - Or with an annotation associated:
>      - **git tag -a v1.1.0 -m "my version 1.1.0"**
>    - Pushing the tag to remote repository witch will associate to the last commit
>      - **git push origin v1.1.0**

<br>

**4: Develop a new feature and implement the same principals as the steps above, with the change in the tag to v1.2.0**

<br>

**5: With the last commit tag it with ca1-part1, marking the end of the first part of the exercise**

<br>

****

### 1.1.2 Second week using branches

<br>

**Part 2**

**1: Using the stable master (main) branch we need to create a new branch to develop new features.**

<br>

**2: Create a new branch (email-field) to develop the new features in parallel to main branch.**

- **Creating the new branch:**
    - There is two ways you can do this.

>- The first way is by firstly creating the branch:
   >  - **git branch email-field**
>
>- Then switch from the main branch to the just created one, you can either use "checkout" or "switch":
   >  - **git checkout email-field**


>- The second way is by creating and moving to the branch in a single command:
   >  - **git checkout -b email-field**

<br>

- **Now that the branch is created, and you are already in it, you can start to implement the new features:**


- After the implementations and tests are done:
    - **git add .**
    - **git commit -m "added new field, support and tests (resolves #7)"**

<br>

- **To push the changes to the remote repository, you have to be in a tracking branch, so we move to main branch to do just that:**
    - **git checkout main**
    - **git push origin email-field**

<br>

- **Since I altered some information inside the README.md from the main branch, I diverged from it and created a conflict in that file:**
- So when I merge the two branches a conflict will arise
    - **git merge email-field**
- To solve this you need to open the file (with nano for example) and select the desired information to keep.
- After altering the information you need to add and commit the changes then push them to the remote repository:
    - **git add .**
    - **git commit -m "solving the conflict of the merge"**
    - **git push origin**
- To conclude this part you need to tag it:
    - **git tag v1.3.0**
    - **git push origin v1.3.0**
- With the merge done, you can remove the branch, you have to remove it both in your local repository and in the remote repository:
    - to remove the branch from your local repository:
        - **git branch -d email-field**
    - to remove the branch from your remote repository:
        - **git push origin --delete email-field**
        
<br>

**3: Create a new branch (fix-invalid-email) to ensure some validations in the employees emails**


- Creating the new branch and move to it:
    - **git checkout -b fix-invalid-email**

<br>

- After the implementations and tests are done:
    - **git add .**
    - **git commit -m "added fixes for email field (resolves #9)"**

<br>

- To push the changes to the remote repository, you have to be in a tracking branch, so we move to main branch to do just that:
    - **git checkout main**
    - **git push origin fix-invalid-email**

<br>

- After the push of the branch to the remote you can merge again with your main, since this time I didn't create any conflicts I only need to do the following:
    - **git merge fix-invalid-email**
    - **git push origin**
    - **git tag v1.3.1**
    - **git push origin v1.3.1**

<br>

- With the merge done, you can remove the branch, you have to remove it both in your local repository and in the remote repository:
    - to remove the branch from your local repository:
        - **git branch -d fix-invalid-email**
    - to remove the branch from your remote repository:
        - **git push origin --delete fix-invalid-email**

<br>

- After doing the alternative to git and finishing the README.md you need to commit the final changes and tag it to ca1-part2
    - Pushing with the final tag:
        - **git tag ca1-part2**
        - **git push origin ca1-part2**

<br>

****

### 1.1.3 Alternative to Git version control system

<br>

**There are a few alternatives to git version control, such as svn (subversion) and mercurial.**

**Both of the alternatives mentioned above have many similarities with git, such as:**
- both are distributed version control systems
- both are free to use
  - svn (subversion) can be used with GitHub remote repository
  - mercurial could be used with bitbucket remote repository, but since 2020 it doesn't support it anymore, so you can use sourceforge.net repository instead
- both have tags as well, to mark the versions of your commits
- in both you can create branches and manage them as well
- and you can merge them too, which completes the steps necessary for the class assignment

<br>

**1: Using mercurial I created a repository in sourceforge.net, you can find it here -> https://sourceforge.net/p/devops-21-22-lmn-1211772-alt/1211772/ci/default/tree/**
>- The commands are very similar to git
>  - **hg add** - instead of **git add**
>  - **hg commit** - instead of **git commit**
>  - **hg push** - instead of **git push**
>  - **hg tag** - instead of **git tag**
>  - **hg branch** - instead of **git branch**
>  - **hg up** - instead of **git checkout/switch**
>  - **hg merge** - instead of **git merge**

<br>

Seeing the commands, the similarities are even more evident, so if you use git it is just a matter of adjusting to these commands.

Implementing was, as I hinted earlier, a bit tricky, I assumed bitbucket was still operating with mercurial, but alas it was not, 
I had to use sourceforge as my remote repository, if you follow the link posted above, you can already see the repository with branches, 
tags and commits, as well as a conflict solved, mimicking the implementation of the class assignment, the only diference was that 
I didn't implement code in java and js, instead I used text files as placeholders for the action, such as the README.md and email-field.txt.

<br>

>- I will summarize some steps to follow. Also, for brevity’s sake I joined the first assignment and the second in the implementation with mercurial.
>- Firstly clone the remote repository:
>  - **hg clone https://joaomarques92@hg.code.sf.net/p/devops-21-22-lmn-1211772-alt/1211772 devops-21-22-lmn-1211772-alt-1211772**
>- I made an initial commit following a guide online on how to start a sourceforge repository, with a README file:
>  - Commit with the README file
>    - **hg add**
>    - **hg commit -m "Initial commit"**
>    - **hg push**
>- Then I created the CA1 directory and the README.md file, and copied the tut-react folder to the CA1 directory, as per assignment 1:
>  - **mkdir CA1 && touch CA1/README.md**
>  - **cp -R tut-react-and-spring-data-rest CA1/**
>- Then I committed the changes. It was at this point i realized mercurial didn't have a staging area as git does:
>  - **hg add**
>  - **hg commit -m "copied tut folder to CA1"**
>  - **hg push**
>- Next I created a new branch:
>  - **hg branch email-field**
>  - **hg push**
>- I switched to default branch:
>  - **hg up default**
>- I made some changes in README.md file inside CA1 directory so a conflict appears, and I could test the conflict management:
>  - **hg add**
>  - **hg commit -m "commit email-field and readme alterations"**
>  - **hg push**
>- Moved back to email-field branch and proceeded to make the alterations in the same line as the README.md file to create the conflict when the two branches merge:
>  - **hg up email-field**
>  - **hg add**
>  - **hg commit -m "commit of changes in README.md to generate conflict between branches"**
>  - **hg push**
>- Moved back to default and merged the two branches:
>  - **hg up default**
>  - **hg merge email-field**
>- A conflict appeared, and it was at this point I decided to experiment with the TortoiseHg mercurial GUI:
>- I tried to add the latest version, but somehow I ended up choosing the original version.
>- Then I pressed the push button in the program, and it worked.
>- Since I failed to accept the change I wanted, I added the change to the .md afterwards and made another commit, this time I used a tag to the commit completing another assignment:
>  - **hg add**
>  - **hg commit -m "merge with email-field and solving conflict with md"**
>  - **hg tag v1.3.1** (when you tag it will automatically commit and attach itself to the last commit, but you still have to push)
>  - **hg push**

<br>

Next follows an image from the TortoiseHg mercurial GUI that illustrates the steps I made.


![](devops-sourceforge.png)

I have a few complaints with using this method, the fact that neither bitbucket nor GitHub support mercurial was a bit disappointing, 
since their sites are much more user-friendly, sourceforge seems a bit outdated and confusing to use, but still I finished the implementation.
Also, I like to add that in some cases I used the TortoiseHg mercurial GUI, just to experiment with the difference between using commands and the GUI.
I found it a bit confusing as well, as I demonstrated earlier with the merging conflicts, but with a few hours invested in it I'm sure I could use it very easily, 
regardless I ended up using much more command lines than the GUI itself.

After implementing and using mercurial I could see that it is very similar to git, it's not that hard learning mercurial if you already know how to use git.

Although you are bound to find the end of the similarities, or the limitations, git has more commands, giving you more leeway and options of use 
(such as deleting branches, something mercurial can't do), at the cost of having a higher learning curve. I found using branches with git more intuitive and easy going, 
with mercurial it was a bit more “hard work” if it makes sense, especially when it comes to the merging of the branches, 
maybe it’s because I already used git before, and I found working with it easier.

The biggest difference, without a doubt, is the fact that git is much more used, which makes it easier to learn or troubleshoot whenever you have a problem
since there is a lot of readily available information. Using mercurial was difficult in the sense that it was harder to find actually meaningful 
and useful information when you encounter an issue.