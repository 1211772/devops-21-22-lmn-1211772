import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Assertions;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class EmployeeTest {

    @Test
    void testTwoEqualObjects() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        Employee employee2 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        assertEquals(employee1, employee2);
    }

    @Test
    void testEqualsSameObject() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        assertEquals(employee1, employee1);
    }

    @Test
    void testEqualsOneObjectIsNull() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        Employee employee2 = null;
        assertNotEquals(employee1, employee2);
    }

    @Test
    void testHashCode() {
        String firstName1 = "Joao";
        String lastName1 = "Marques";
        String description1 = "Student";
        String jobTitle1 = "Switch Student";
        String email1 = "joao@email.com";
        int jobYears1 = 10;
        Employee employee1 = new Employee(firstName1, lastName1, description1, jobTitle1, jobYears1, email1);
        String firstName2 = "Jose";
        String lastName2 = "Martins";
        String description2 = "Citizen";
        String jobTitle2 = "Farmer";
        String email2 = "jose@email.com";
        int jobYears2 = 20;
        Employee employee2 = new Employee(firstName2, lastName2, description2, jobTitle2, jobYears2, email2);
        Employee x = employee1;
        assertNotEquals(employee1, employee2);
        assertEquals(employee1.hashCode(), x.hashCode());
    }

    @Test
    void getFirstName() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        String expected = "Joao";
        String result = employee1.getFirstName();

        assertEquals(expected, result);
    }

    @Test
    void getLastName() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        String expected = "Marques";
        String result = employee1.getLastName();
        assertEquals(expected, result);
    }

    @Test
    void getDescription() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        String expected = "Student";
        String result = employee1.getDescription();
        assertEquals(expected, result);
    }

    @Test
    void getJobTitle() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        String expected = "Switch Student";
        String result = employee1.getJobTitle();
        assertEquals(expected, result);
    }

    @Test
    void getJobYears() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        int expected = 10;
        int result = employee1.getJobYears();
        assertEquals(expected, result);
    }

    @Test
    void getEmail() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        String expected = "joao@email.com";
        String result = employee1.getEmail();
        assertEquals(expected, result);
    }

    @Test
    void createEmployeeFailsFirstNameNull() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "";
            String lastName = "Marques";
            String description = "Student";
            String jobTitle = "Switch Student";
            String email = "joao@email.com";
            int jobYears = 10;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("First name cannot be empty", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsLastNameNull() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "";
            String description = "Student";
            String jobTitle = "Switch Student";
            String email = "joao@email.com";
            int jobYears = 10;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Last name cannot be empty", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsDescriptionNull() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "Marques";
            String description = "";
            String jobTitle = "Switch Student";
            String email = "joao@email.com";
            int jobYears = 10;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Description cannot be empty", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsJobTitleNull() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "Marques";
            String description = "Student";
            String jobTitle = "";
            String email = "joao@email.com";
            int jobYears = 10;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Job Title cannot be empty", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsEmailNull() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "Marques";
            String description = "Student";
            String jobTitle = "Switch Student";
            String email = "";
            int jobYears = 10;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Invalid email", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsEmailInvalidEmailNoDot() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "Marques";
            String description = "Student";
            String jobTitle = "Switch Student";
            String email = "joao@emailcom";
            int jobYears = 10;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Invalid email", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsEmailInvalidEmailNoAtSign() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "Marques";
            String description = "Student";
            String jobTitle = "Switch Student";
            String email = "joaoemail.com";
            int jobYears = 10;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Invalid email", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsJobYearsZero() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "Marques";
            String description = "Student";
            String jobTitle = "Switch Student";
            String email = "joao@email.com";
            int jobYears = 0;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Job Years needs to be a positive number", thrown.getMessage());
    }

    @Test
    void createEmployeeFailsJobYearsNegative() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Joao";
            String lastName = "Marques";
            String description = "Student";
            String jobTitle = "Switch Student";
            String email = "joao@email.com";
            int jobYears = -1;
            Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        });
        Assertions.assertEquals("Job Years needs to be a positive number", thrown.getMessage());
    }

    @Test
    void testToString() {
        String firstName = "Joao";
        String lastName = "Marques";
        String description = "Student";
        String jobTitle = "Switch Student";
        String email = "joao@email.com";
        int jobYears = 10;
        Employee employee1 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        String expected = "Employee{id=null, firstName='Joao', lastName='Marques', description='Student', jobTitle='Switch Student', jobYears='10', email='joao@email.com'}";
        String result = employee1.toString();
        assertEquals(expected, result);
    }

}
