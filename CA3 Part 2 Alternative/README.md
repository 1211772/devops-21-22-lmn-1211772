# DEVOPS

## 1. Class Assignment 3

### 1.1 CA3, Part 2 Alternative - Virtualization with Vagrant

<br>

**What is virtualization?**

Virtualization is the process of running a virtual instance of a computer system in a layer abstracted from the actual 
hardware. Most commonly, it refers to running multiple operating systems on a computer system simultaneously. To the 
applications running on top of the virtualized machine, it can appear as if they are on their own dedicated machine, 
where the operating system, libraries, and other programs are unique to the guest virtualized system and unconnected to 
the host operating system which sits below it.

<br>

**What is a hypervisor?**

A hypervisor is a program for creating and running virtual machines. Hypervisors have traditionally been split into 
two classes: type one, or "bare metal" hypervisors that run guest virtual machines directly on a system's hardware, 
essentially behaving as an operating system. Type two, or "hosted" hypervisors behave more like traditional applications 
that can be started and stopped like a normal program.

<br>

**What is a virtual machine?**

A virtual machine is the emulated equivalent of a computer system that runs on top of another system. Virtual machines 
may have access to any number of resources: computing power, through hardware-assisted but limited access to the host 
machine's CPU and memory; one or more physical or virtual disk devices for storage; a virtual or real network interface; 
as well as any devices such as video cards, USB devices, or other hardware that are shared with the virtual machine.

<br>

**VirtualBox?**

VirtualBox is open-source software for virtualizing the x86 computing architecture. It acts as a hypervisor, creating a 
VM (virtual machine) where the user can run another OS (operating system).

The operating system where VirtualBox runs is called the "host" OS. The operating system running in the VM is called the 
"guest" OS. VirtualBox supports Windows, Linux, or macOS as its host OS.

When configuring a virtual machine, the user can specify how many CPU cores, and how much RAM and disk space should be 
devoted to the VM. When the VM is running, it can be "paused." System execution is frozen at that moment in time, and 
the user can resume using it later.

<br>

**VMware?**

VMware is a virtualization and cloud computing software, it bases its virtualization technologies on its bare-metal 
hypervisor ESX/ESXi in x86 architecture. Bare-metal embedded hypervisors can run directly on a server’s hardware without 
the need of a primary operating system. With VMware server virtualization, a hypervisor is installed on the physical 
server to allow for multiple virtual machines (VMs) to run on the same physical server. Each VM can run its own operating 
system, allowing multiple OSes to run on one physical server. All of the VMs on the same physical server share resources, 
such as networking and RAM.

<br>

**VirtualBox vs VMware**

| VirtualBox                                                                     | VMware                                    | 
|--------------------------------------------------------------------------------|-------------------------------------------|
| Provides virtualization for both hardware and software                                                 | Provides virtualization only for hardware.
| Is widely available to many OS                                                                         | VMware supports a narrow scope of OS.
| Supports VDI (Virtual Disk Image), Virtual Machine Disk (VMDK), Virtual Hard Disk (VHD).               | Support Virtual Machine (VMDK)
| More user friendly interface                                                                           | A bit more complicated to get the hang of it
| VirtualBox is a Type 2 Hypervisor.                                                                     | VMware ESXi is a Type 1 Hypervisor that works directly on the host machine’s hardware resources. Some VMware products, such as VMware Player, VMware Workstation, and VMware Fusion, are Type 2 Hypervisors.
| Shared folders are available in Oracle VirtualBox. 	                                                 | Shared folders are available in VMware products such as VMware Workstation, VMware Player, and VMware Fusion. Virtual machines that run on an ESXi host are not supported, and shared files must be manually built.
| It can only support 3D graphics Up to OpenGL 3.0 and Direct3D 9. 	                                     | VMware provides 3D graphics with DirectX 10 and OpenGL 3.3 support for all of its products.
| Video memory, in case of VirtualBox, is limited to 128 MB. 	                                         | Video memory, in case of VMware, is limited to 2 GB.
| To support 3D graphics in VirtualBox, you must manually enable 3D acceleration on the virtual machine. | 3D acceleration is enabled by default.
| The following network modes are available in VirtualBox:Not attached, Network Address Translation (NAT), NAT Network, Bridged networking, Internal networking, Host-only networking, Generic networking, UDP Tunnel, Virtual Distributed Ethernet (VDE)  | VMware supports the following network nodes:Network Address Translation (NAT), Host-only networking, Virtual network editor (on VMware workstation and Fusion Pro)
| It is an open source tool. 	                                                                         | It is an open source tool.


<br>

**Vagrant**

Vagrant is a tool for building and managing virtual machine environments in a single workflow. With an easy-to-use 
workflow and focus on automation, Vagrant lowers development environment setup time, increases production parity, and 
makes the "works on my machine" excuse a relic of the past.

If you are already familiar with the basics of Vagrant, the documentation provides a better reference build for all 
available features and internals.


<br>

**1: Starting the exercise (CA3 Part 2 Alternative):**

**Create CA3 Part 2 Alternative folder and README.md file**

- **Creating the CA3 Part 2 Alternative directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>    - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA3 Part 2 Alternative\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA3 Part 2 Alternative folder and Readme.md (resolves #32)"`**
>  - **`git push`**

<br>

**2: Installing and initial configurations for VMware**

- **Downloading and installing VMware**
>- To download use the following link (https://customerconnect.vmware.com/en/downloads/details?downloadGroup=WKST-PLAYER-1623-NEW&productId=1039&rPId=85399)
>- Installing is simple, just follow the steps

- **It is necessary to install the package Vagrant VMware Utility found in Vagrant's site**
>- To download use the following link (https://www.vagrantup.com/vmware/downloads)
>- Installing is simple, just follow the steps

- **Now we install the plugin for the VMware provider**
>- **`vagrant plugin install vagrant-vmware-desktop`**

- **It is always safe to run the update of the plugin after installing, to make sure we are up-to-date on the releases**
>- **`vagrant plugin update vagrant-vmware-desktop`**

<br>

**3: Configuring Vagrantfile**

- **You can either create a new one in CA3 Part 2 Alternative directory:**
>- **`vagrant init`**
>- **`vagrant box add hashicorp/bionic64`**

- **Or just copy the Vagrantfile from CA3 Part 2 to the CA3 Part 2 Alternative folder:**
>- **`Copy-Item "D:\devops-21-22-lmn-1211772\CA3 Part 2\Vagrantfile" -Destination "D:\devops-21-22-lmn-1211772\CA3 Part 2 Alternative"`**

<br>

**In my case I opted for the former and did the following alterations to the Vagrantfile:**

I need to define the box, and its version, that will be used for both VMs so, in the Vagrantfile from line 3 and 4 change to:

```Vagrantfile
Vagrant.configure("2") do |config|
   config.vm.box = "hashicorp/bionic64"
   config.vm.box_version = "1.0.282"
```

- **Also in each VM alter the box as well:**
- **_In the Vagrantfile db VM:_**
>  - Line 20 change to : **`db.vm.box = "hashicorp/bionic64"`**
- **_In the Vagrantfile web VM:_**
>  - Line 47 change to : **`web.vm.box = "hashicorp/bionic64"`**

- **Also alter the provider to be VMware:**
- **_In the Vagrantfile web VM:_**
>  - Line 52 change to : **`web.vm.provider "vmware_desktop" do |v|`**

<br>

- **Networking configuration:**
- **_We need to alter the IP for each VM because VMware creates a specific Adapter with a specific IP:_**
>- Go to Control Panel / Network and Internet / Network Connections
>- Double-click "VMware Virtual Ethernet Adapter for VMnet1"
>- Click on the "Details" button to check which IP is this network using, in my case : "192.168.142.1"

- **_In the Vagrantfile db VM:_**
>  - Line 22 change to : **`db.vm.network "private_network", ip: "192.168.142.11"`**

- **_In the Vagrantfile web VM:_**
>  - Line 49 change to : **`web.vm.network "private_network", ip: "192.168.142.10"`**

<br>

- **Configuring application.properties:**
>- Inline with CA3 Part 2, here we need to do some alterations to application.properties as well:
>- Line 5 change to : **`spring.datasource.url=jdbc:h2:tcp://192.168.142.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE`**

<br>

- **Using the app**
>- Executing the app : **`vagrant up`**

- **_Checking if the web VM is working_**
>- **`http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT`**
>- **`http://192.168.142.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT`**

- **_Checking if the db VM is working_**
- To access the database in the "JDBC URL" field type/paste the following : **`jdbc:h2:tcp://192.168.142.11:9092/./jpadb`**
>- **`http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`**
>- **`http://192.168.142.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console`**

- **_In the db I inserted the following to check if it was working_**
- **`insert into employee (id, first_name, last_name, description) values ( 2, 'Gandalf', 'The Grey', 'Sorcerer')`**

As you can see by the image below the implementation of the alternative worked.

![](Alternative Works.png)

**4: Conclusion**

Both methods of virtualization are quite simple to use, and somewhat similar too, apart for some differences in configuration,
mainly the network one, as described earlier.
From research, I found that VMware is more utilized in professional environments, maybe because they have more choice in what 
application to use, for example they provide both type 1 and 2 Hypervisor. VirtualBox from what I gathered is more utilized in
home environments, or for academic purposes. Both are opensource, but VMware only has one option for a free version (the one I linked earlier) 
, and several paid options, VirtualBox is free.

In terms of the difference of use in regard to the assignments they were very similar to use apart for the differences 
presented along this document, the use of the Vagrantfile was what made it very easy and not bothersome to do the configurations
one by one. When implementing the alternative I really understood and appreciated the use of the Vagrantfile, because I 
changed the entire method of running the application, but with the Vagrantfile already configured to run the application, 
the change in the method of virtualization didn't require much change to the Vagrantfile, as you can see from this document.

