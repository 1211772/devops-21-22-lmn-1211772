# DEVOPS

## 1. Class Assignment 5

### 1.1 CA5, Part 1 - CI/CD Pipelines with Jenkins

<br>

**Starting the exercise (CA5 Part 1):**

**Create CA5 Part 1 folder and README.md file**

- **Creating the CA5 Part 1 directory, README.md:**
>- **_Firstly open the PowerShell_**:
   >  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
        >    - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA5 Part 1\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA5 Part 1 file and Readme, and altered the CA2 Part 1 needed for the assignment (resolves #48)"`**
>  - **`git push`**

<br>

**1: The goal of the Part 1 of this assignment is to practice with Jenkins using the "gradle
basic demo" project that should already be present in the student’s individual
repository.**

- **The goal is to create a very simple pipeline (similar to the example from the lectures):**
>  - **Note that this project should already be present in a folder inside your individual repository!**
>  - **When configuring the pipeline, in the Script Path field, you should specify the relative
     path (inside your repository) for the Jenkinsfile. For instance,
     ’ca2/part1/gradle-basic-demo/Jenkinsfile’**

- **Setting up Jenkins:**
>  - **Download jenkins.war from -> https://www.jenkins.io/download/**
>  - **Open PowerShell in the folder that you saved the downloaded file**
>  - **Execute `java -jar jenkins.war`** (this command by deafult uses the port :8080, because, 
     from previous Class Assingments, this port is already taken, we have to execute a 
     different command to force the jenkins to run in a specific port
>  - **Execute `java -jar jenkins.war --httpPort=9090`**

- **Setting a new pipeline job:**
>  - **Open `http://localhost:9090` to access Jenkins**
>  - **Go to `Dashboard`**
>  - **Select `New Item`**
>  - **Naming the job `Name:`** (I named it "nome")
>  - **Click on `Pipeline` and then `OK`**

- **To create the pipeline from a Jenkinsfile, follow these steps:**
>  - **Click on `Pipeline`**
>  - **In `Definition`**
>  - **Choose the option `Pipeline script from SCM`**
>  - **In defining the `Source Control Manager` choose `Git`**
>  - **Paste your repository URL: https://bitbucket.org/1211772/devops-21-22-lmn-1211772**
>  - **Since our main branch isn't the default `*/master`, specify which branch is your main one `*/main`**
>  - **Specify the script path: `CA2 Part 1/gradle_basic_demo/Jenkinsfile`**

<br>

**2: You should define the following stages in your pipeline**

- **Checkout.** To checkout the code form the repository
- **Assemble.** Compiles and Produces the archive files with the application. Do not use the build 
task of gradle (because it also executes the tests)!
- **Test.** Executes the Unit Tests and publish in Jenkins the Test results. See the junit step 
for further information on how to archive/publish test results.
- Do not forget to add some unit tests to the project (maybe you already have done it).
- **Archive.** Archives in Jenkins the archive files (generated during Assemble)

<br>

- **Creating the Jenkinsfile with the aforementioned stages:**
>  - **In Intellij**
>  - **In `CA2 Part 1/gradle_basic_demo/`**
>  - **Create new file `Jenkinsfile` without any extension**

<br>

- **The Jenkinsfile should look like this:**

```Jenkinsfile
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'main', url: 'https://bitbucket.org/1211772/devops-21-22-lmn-1211772'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    bat './gradlew clean assemble'
                    }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    bat './gradlew test'
                    }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA2 Part 1/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*'
                    }
            }
        }
    }
    post {
        always {
            junit 'CA2 Part 1/gradle_basic_demo/build/test-results/test/*.xml'
            }
    }
}
```

<br>

- **Added unit tests to: `CA2 Part 1/gradle_basic_demo/src/test/basic_demo/AppTest.java`**

```java
public class AppTest {

     @Test public void testAppHasAGreeting() {
          App classUnderTest = new App();
          assertNotNull("app should have a greeting", classUnderTest.getGreeting());
     }

     @Test public void oneEqualsOne() {
          int expected = 1;
          int result = 1;
          assertEquals(expected, result);
     }
}
```

<br>

**3: Excuting the build and see the results in Jenkins**

- **Open `http://localhost:9090` to access Jenkins**
- **Choose the Job `nome`**
- **Click on `Build Now`**
- **One of the builds failed because I use spaces in the directory names, and I forgot to correctly name 
the paths in the Jenkinsfile, so after that alteration, as you can see by the image below, the builds worked 
as intended, and it showed the `Test Result Trend`** aswell.

![](jenkins.PNG)

<br>

**4: Final commit with changes to readme:**

>- **_Commit the changes and push:_**
>  - **`git add .`**
>  - **`git commit -m "Final changes to readme file (resolves #50)"`**
>  - **`git push`**
>  - **`git tag ca5-part1`**
>  - **`git push origin ca5-part1`**