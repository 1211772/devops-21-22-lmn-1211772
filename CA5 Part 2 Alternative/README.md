# DEVOPS

## 1. Class Assignment 5

### 1.1 CA5, Part 2 Alternative - CI/CD Pipelines with Jenkins

<br>

**1: Starting the exercise (CA5 Part 2 Alternative):**

**Create CA5 Part 2 Alternative folder and README.md file**

- **Creating the CA5 Part 2 Alternative directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>    - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA5 Part 2 Alternative\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "Created CA5 Part 2 Alternative folder and Readme.md (resolves #55)"`**
>  - **`git push`**

<br>

**2: For this assignment you should present an alternative technological solution for the continuous integration tool 
(i.e., an alternative to Jenkins).**

- **Jenkins:**
>  - Jenkins is an open-source automation tool written in Java with plugins built for Continuous 
Integration purposes.
>  - Jenkins is used to build and test your software projects continuously making it easier for 
developers to integrate changes to the project, and making it easier for users to obtain a fresh build.
>  - It also allows you to continuously deliver your software by integrating with a large number of 
testing and deployment technologies.
>  - Jenkins achieves Continuous Integration with the help of plugins. Plugins allow the integration of 
Various DevOps stages.
>  - If you want to integrate a particular tool, you need to install the plugins for that tool.

<br>

- **For the alternative options there are severall of them, we will be discussing some of them:**
>  - **Buddy**
>  - **FinalBuilder**
>  - **GoCD**
>  - **GitLab CI**

<br>

**3: Talking alternatives:**

- **FinalBuilder:**
>  - FinalBuilder is a commercial Windows build automation tool that provides a unified graphical 
interface to author and execute build projects.
>  - It is written in Delphi, C# (FinalBuilder hosts the Microsoft .NET CLR), VBScript, and JScript.
>  - The tool has a mild bias towards building Delphi projects, but it supports a range of compilers 
making it suitable for building projects in many languages.
>  - It has a 30-day free trial, so I tried to use it as an alternative to Jenkins, but found it very 
difficult to use, and the GUI wasn't user-friendly at all.

<br>

- **GitLab CI:**
>  - Is a tool for software development using the continuous methodologies: (Continuous Integration (CI),
Continuous Delivery (CD), Continuous Deployment (CD).
>  - It can automatically build, test, deploy, and monitor your applications by using Auto DevOps.
>  - It uses a .yml file to configure the pipeline, with different stages and jobs.
>  - I tried this alternative too, since it said it had a free trial too, but when I whent to the 
CI/CD part it asked me for a credit card to continue.

<br>

- **GoCD:**
>  - GoCD is an open-source tool which is used in software development to help teams and organizations
     automate the continuous delivery (CD) of software.
>  - It supports automating the entire build-test-release process from code check-in to deployment.
>  - It supports several version control tools including Git, Mercurial, Subversion, Perforce and TFVC.
>  - Other version control software can be supported by installing additional plugins.
>  - I tried this alternative but for some reason the agent wasn't functioning properly, it was enabled 
but when running the stages it said `the job is currently running, but it has not been assigned an agent 
in the last 5 minute(s). This job may be hung.` 

**Since I couldn't make it work on any of the alternatives I tried one last time with Buddy.**

<br>

- **Buddy:**
>  - Is a web-based and self-hosted continuous integration (CI) and delivery software for Git developers 
that can be used to build, test and deploy websites and applications with code from GitHub, 
Bitbucket and GitLab.
>  - It employs Docker containers with pre-installed languages and frameworks for builds, alongside DevOps, 
monitoring and notification actions.


<br>

- **Differences between Jenkins and Buddy:**

|                      | Jenkins                                                    | GoCD                                                                              | 
|:---------------------|------------------------------------------------------------|-----------------------------------------------------------------------------------|
| Effort               | Requires installation and regular maintenance.             | No installation or maintenance needed.                                            |
| Integrations         | Open source plugins.                                       | Pre-made and tested integrations.                                                 |
| Pipelines            | Scripted and UI driven.                                    | Scripted and UI driven.                                                           |
| Pricing              | Free (discounting effort cost).                            | Subscription based. (Or free up to 5 projects)                                    |
| Customisation        | High degree of control but varying quality.                | Mny, highly tested options to choose from but little control on new integrations. |
| Specialist Knowledge | Requires at least a good working understanding of Jenkins. | None needed.                                                                      |

<br>

- **For more details on both of them and their diferrences here is the full documentation consulted:**
>  - Creating pipelines with both Jenkins and Buddy -> https://medium.com/the-devops-corner/jenkins-and-buddy-c629d5d5e564
>  - Differences between both of them -> https://hackernoon.com/buddy-vs-jenkins-mt4v32u2
>  - More differences between both of them from Buddy's website-> https://buddy.works/compare/jenkins-alternative
>  - Full documentation for Buddy -> https://buddy.works/docs

<br>

**4: Possible implementation of Buddy:**

**Firstly we need to create a Buddy account:**
>  - Go to -> https://buddy.works

**Creating a new pipeline:**
>  - **Dashboard** select `Create new project`
>  - **Select a repository** in my case I selected the devops Bitbucket repository (https://bitbucket.org/1211772/devops-21-22-lmn-1211772/src/main/)
>  - Select **Add new pipeline** and fill in the blanks
>  - **Name:** ca5_part2_alternative
>  - **Purpose:** select `CI/CD: Build, test, deploy`
>  - **Trigger:** pick `Manually`
>  - **Branch:** choose `main`
>  - Click on **Add pipeline**

**Since Buddy as a user-friendly interface and you can export your build afterwards to a yaml file, wich 
you can then put it in your repository, I will go with this approach.**

**I tried with the GUI but for some reason it was having issues with the name of tmy folders and the archiving 
stage wasn't working, wich means the docker image and push wasn't working either, but up until then everything 
was working.**

**See the following images for the error**

![](IMG-20220606-WA0010.jpeg)

![](IMG-20220606-WA0012.jpeg)

**For that reason I couldn't conclude the exercise but the yaml file should look something like this:**

```yml
- pipeline: "ca5_part2_alternative"
on: "CLICK"
refs:
     - "refs/heads/main"
priority: "NORMAL"
fail_on_prepare_env_warning: true
actions:
     - action: "Assemble"
       type: "BUILD"
       working_directory: "/buddy/devops-21-22-lmn-1211772"
       docker_image_name: "library/gradle"
       docker_image_tag: "latest"
       execute_commands:
            - "cd CA2 Part 1/gradle_basic_demo"
            - "chmod +x gradlew"
            - "./gradlew clean assemble"
       volume_mappings:
            - "/:/buddy/devops-21-22-lmn-1211772"
       cache_base_image: true
       shell: "BASH"
     - action: "Test"
       type: "BUILD"
       working_directory: "/buddy/devops-21-22-lmn-1211772"
       docker_image_name: "library/gradle"
       docker_image_tag: "latest"
       execute_commands:
            - "cd CA2 Part 1/gradle_basic_demo"
            - "./gradlew test"
       volume_mappings:
            - "/:/buddy/devops-21-22-lmn-1211772"
       cache_base_image: true
       shell: "BASH"
     - action: "Jar"
       type: "BUILD"
       working_directory: "/buddy/devops-21-22-lmn-1211772"
       docker_image_name: "library/gradle"
       docker_image_tag: "latest"
       execute_commands:
            - "cd CA2 Part 1/gradle_basic_demo"
            - "./gradlew jar"
       volume_mappings:
            - "/:/buddy/devops-21-22-lmn-1211772"
       cache_base_image: true
       shell: "BASH"
     - action: "Javadoc"
       type: "BUILD"
       working_directory: "/buddy/devops-21-22-lmn-1211772"
       docker_image_name: "library/gradle"
       docker_image_tag: "latest"
       execute_commands:
            - "cd CA2 Part 1/gradle_basic_demo"
            - "./gradlew javadoc"
       volume_mappings:
            - "/:/buddy/devops-21-22-lmn-1211772"
       cache_base_image: true
       shell: "BASH"
     - action: "Archive"
       type: "ZIP"
       local_path: "CA2 Part 1/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar"
       destination: "CA2 Part 1/gradle_basic_demo/build/distributions/*.zip"
       deployment_excludes:
            - ".git"
     - action: "Build Docker image"
       type: "DOCKERFILE"
       dockerfile_path: "CA2 Part 1/gradle_basic_demo/Dockerfile"
       target_platform: "linux/amd64"
     - action: "Push Docker image"
       type: "DOCKER_PUSH"
       docker_image_tag: "ca5_part2_alternative"
       repository: "1211772/ca5_part2_alternative"
       integration_hash: "since I couldn't go this far I don't know the hash"
```

<br>

**Since trying to fix the issue in the archiving step I'm afraid I changed things in CA2 Part 1, and may no longer 
be working as intended, wich I used in CA5 Part 2, but as you could analise, and with photografic evidence 
I completed the CA5 Part 2 assingment.**

<br>

**5: Conclusion and final commit:**

**I had a lot of trouble trying to implement the alternative with several ways, FinalBuilder was too confusing, 
GitLab asked me for a credit card to proceed with the CI/CD, and GoCD wasn't, dispite being able to configure it, 
I wasn't able to run the pipeline due to the issue with the agent, wich I found no answer to, so I ultimately 
moved to Buddy, my last chance.**

**I found Buddy to be a lot intuitive and easier to work on, easier than Jenkins for sure, it has a great and 
user-friendly interface witch helps along the way.**

**The fact that I encountered an issue when making the pipeline and couldn't find usefull help, at least with the 
time constrainsts I was working on, says a lot about how many people use, and it says even more about the stronghold 
Jenkins has on the market, with companies like Google and Netflix being one of the heavy hitters that use the platform.**

**I think Buddy has a future, but for now, at least in near future, Jenkins is more complicated to get into, and to 
configure fully to your parameters, but has a lot more support for troubleshooting issues.**
