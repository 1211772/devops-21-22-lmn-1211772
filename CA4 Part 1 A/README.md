# DEVOPS

## 1. Class Assignment 4

### 1.1 CA4, Part 1 - Containers with Docker

<br>

**1: Starting the exercise (CA4 Part 1 A):**

**Create CA4 Part 1 A folder and README.md file**

- **Creating the CA4 Part 1 A directory, README.md:**
>- **_Firstly open the PowerShell_**:
>  - Move to the repository (In my case it's here (d/devops-21-22-lmn-1211772))
>  - `cd D:\devops-21-22-lmn-1211772\`

- **Creating the directory and the file:**
>  - **`New-Item -Path 'D:\devops-21-22-lmn-1211772\CA4 Part 1 A\README.md' -ItemType File -Force`**

- **Committing to the repository:**
>  - **`git add .`**
>  - **`git commit -m "created folders and readme's for ca4 part 1 (resolves #35)"`**
>  - **`git push`**

<br>

**2: Installing Docker Desktop:**

>- **_Download from the following site:_**
>  - https://www.docker.com/products/docker-desktop/
>  - click the word "Windows" in the line (Also available for **Windows** and Linux)

>- **_After installing Docker Desktop a popup will appear to prompt you to install WSL 2:_**
>  - That popup has a link, click the link and follow the guide
>  - Reboot your PC

>- **_Create an account in Docker Hub_**

<br>

**3: Set up a Dockerfile:**

- **_In this exercise it's supposed to build the chat server "inside" the Dockerfile_**

>- **_Creating a Dockerfile:_**
>  - In the CA4 Part 1 A folder in IntelliJ
>  - Create a file named Dockerfile
>  - Start writting what you want in the file (**FROM, RUN, WORKDIR, EXPOSE, CMD**)

```Dockerfile
# Starting with base image
FROM ubuntu:18.04

# Install dependencies
RUN apt-get update -y
RUN apt-get install git -y
RUN apt-get install openjdk-11-jdk-headless -y

# Clone the repository into the docker container
RUN git clone https://1211772@bitbucket.org/luisnogueira/gradle_basic_demo.git

# Go to the root of the project
WORKDIR gradle_basic_demo/

# Give the required permissions to execute the application
RUN chmod u+x gradlew

# Run the clean task to remove the build folder (if exists) thus cleaning everything including leftovers from previous builds which are no longer relevant.
RUN ./gradlew clean build

# Expose port
EXPOSE 59001

# Execute the container
CMD java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

<br>

**4: Creating an image:**

>- **_In Powershell:_**
>  - **`cd D:\devops-21-22-lmn-1211772\'CA4 Part 1 A'\`**
>  - **`docker build -t ca4part1a .`**

<br>

**5: Creating and running a container:**

>- **_In Powershell:_**
>  - **`docker run --name container_1 -p 59001:59001 -d ca4part1a`**
>  - the command --name lets me choose the name I want for my container by typing it next to the command
>  - the command -d lets me choose the name I want for my image by typing it next to the command

<br>

**6: Tagging the image and publishing it on Docker Hub:**

>- **_In Powershell:_**
>  - in the CA4 Part 1 A folder
>  - **`docker login`**
>  - **`docker tag ca4part1a 1211772/ca4part1a:1.0`** (docker tag image user/image:tag_version)
>  - **`docker push 1211772/ca4part1a:1.0`** (docker push user/image:tag_version)

![](push to repo.PNG)

<br>

**7: Executing the chat client on the host machine:**

>- **_While running the container with the server running:_**
>  - in another Powershell go to `cd D:\gradle_basic_demo` (wich is a clone of the professors' repository -> https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/)
>  - **`./gradlew runClient`**
>  - the chat application is working as intended *

- -* I had an issue here where the chat application would appear, but it wouldn't work properly, then I removed all the images and all the containers and when I repeated the process it would give me an error that the port 59001 was already in use, which it wasn't. 
- Since I did that in my laptop, I ran all the steps in my desktop and everything worked fine, meaning I don't quite know what the issue was, but if you follow the steps above, like I did in my desktop, everything will work.

<br>

**8: Committing everything to the repository and finalizing CA4 Part 1 A:**

>  - **`git add .`**
>  - **`git commit -m "committing files and updating readme (resolves #40)"`**
>  - **`git push`**
>  - **`git tag ca4-part1a`**
>  - **`git push origin ca4-part1a`**
